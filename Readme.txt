Tcr Receptor Utilities for Solid Tissue (TRUST) is a python standalone tool to analyze TCR sequences using unselected RNA sequencing data, profiled from solid tissues, including tumors. TRUST performs de novo assembly on the hypervariable complementarity-determining region 3 (CDR3) and reports contigs containing the CDR3 DNA and amino acid sequences. TRUST then realigns the contigs to IMGT reference gene sequences to report the corresponding variable (V) or joining (J) genes. Currently, TRUST only supports paired-end sequencing data with sufficient insert size (insert size > 2*read_length). Questions or suggestions should be addressed to Bo Li (bli@jimmy.harvard.edu). TRUST is developed by Bo Li in Shirley Liu lab, with all rights reserved. 

## Prerequisites for TRUST

Before using TRUST, make sure the following python packages are properly installed:
pysam
Bio
gmpy (optional)
numpy
itertools
optparse

## Data files

Along with the source code (two .pyc files), please also download the data.zip file and unzip it to the same directory as you place the souce code files. 

## Input files

TRUST takes BAM files as input. Please make sure that each BAM is paired with its index file, ending with .bam.bai
BAM file should be aligned to hg19 human reference genome

## Input modes

TRUST supports 3 input modes, to accommodate multiple file inputs.

-d option processes all the BAM files in a given directory
-F option processes all the files listed in a given file list (in a txt file)
-f option processes a single BAM file

## General usage

python ExtractReads.pyc YOUR_BAM_FILE.bam
python TRUSTcf.pyc -f YOUR_BAM_FILE.bam-Locs.bam -a
 
## Fields of fasta info line
File name
10 digits random ID
TCR genes and locations (based on mapped reads)
Estimated clonal frequency: the number of reads used to assemble the contig divided by contig length
Contig length
Number of reads in the TCR regions
TCR gene (based on alignment to IMGT reference genes)
CDR3 amino acid sequence
Minus log e-value: E-value for IMGT reference alignment
CDR3 DNA sequence